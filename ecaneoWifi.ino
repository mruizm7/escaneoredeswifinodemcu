
#include "ESP8266WiFi.h"

const int RSSI_MAX =-50;// define maximum straighten of signal in dBm
const int RSSI_MIN =-100;// define minimum strength of signal in dBm


int openNetworkCount=0;// No lo cambies. Se usa en el método loop

void setup() {
  Serial.begin(115200);
  Serial.println("Wifi Signal Scan");
  // Configura WiFi a modo estación y desconecta desde un AP if it was previously connected
  WiFi.mode(WIFI_STA);
  WiFi.disconnect();
  delay(2000);

  Serial.println("Configuración Terminada");
}

void loop() {
  Serial.println("Inicia escaneo de redes Wifi");

  // WiFi.scanNetworks regresa el número de redes encontradas
  int n = WiFi.scanNetworks();
  Serial.println("Escaneo Wifi Terminado");
  if (n == 0) {
    Serial.println("No se encontro ninguna red Wifi");
  } else {
    Serial.print(n);
    Serial.println(" Redes WiFi encontradas:");
    for (int i = 0; i < n; ++i) {
      // Imprime el SSID y RSSI para cada red encontrada
      if(WiFi.encryptionType(i) == ENC_TYPE_NONE)
      {
      openNetworkCount++;
      Serial.print(openNetworkCount);
      Serial.print(") ");
      Serial.print(WiFi.SSID(i));// SSID
      
                  
      Serial.print(WiFi.RSSI(i));//Fuerza de la señal en dBm  
      Serial.print("dBm (");

      
      Serial.print(dBmtoPercentage(WiFi.RSSI(i)));//Fuerza de la señal en %  
      Serial.println("% )");
      }// if 


      delay(10);
    }
  }
  Serial.println("");

  // Espera un poco antes de escanear de nuevo
  delay(10000);
  WiFi.scanDelete();  
openNetworkCount =0;//Reinicia el contador de las redes  
}// loop



/*
 * Fuente de adaptación de código fue posible gracias al programador::::
 * Ahmad Shamshiri
 * with lots of research, this sources was used:
 * https://support.randomsolutions.nl/827069-Best-dBm-Values-for-Wifi 
 * This is approximate percentage calculation of RSSI
 * Wifi Signal Strength Calculation
 * Written Aug 08, 2019 at 21:45 in Ajax, Ontario, Canada
 */

int dBmtoPercentage(int dBm)
{
  int quality;
    if(dBm <= RSSI_MIN)
    {
        quality = 0;
    }
    else if(dBm >= RSSI_MAX)
    {  
        quality = 100;
    }
    else
    {
        quality = 2 * (dBm + 100);
   }

     return quality;
}//dBm a Porcentaje